import Vue from 'vue';
import App from './App';
import router from './router';
import axios from 'axios';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';    // 默认主题
// import '../static/css/theme-green/index.css';       // 浅绿色主题
import '../static/css/theme-green/mui.css';       // 自定义图标
import '../static/css/iconfont/iconfont.css';       //阿里图标
import '../static/css/icon.css';

import "babel-polyfill";
import global_ from './components/Global'; //引用全局配置文件
import httpUtil_ from './components/util/HttpUtil'; //引用全局配置文件
import VueJsonp from 'vue-jsonp'


Vue.prototype.Global=global_;

Vue.prototype.HttpUtil=httpUtil_;
// axios.defaults.baseURL = 'http://120.78.78.201:7009/';
axios.defaults.baseURL = 'http://192.168.2.122:7009/';
axios.defaults.withCredentials=true;//每次请求携带cookie
// axios.defaults.baseURL = 'http://192.168.2.105:8999/';
Vue.use(ElementUI, { size: 'small' });
Vue.prototype.$axios = axios;
Vue.use(VueJsonp)
//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    let isLogin = sessionStorage.getItem('isLogin');
    const role = isLogin == null || isLogin == undefined ? undefined : isLogin;


    let userMap = sessionStorage.getItem('userMap');

    // const role = userMap == null || userMap == undefined ? undefined : JSON.parse(userMap).userName;
    //存放请求医院的标志
    //let hospital_cat = sessionStorage.getItem('hospital_cat');
    const path = to.redirectedFrom == null ? "/login" :to.redirectedFrom.toString().replace("/","");
  
    
    if(!role&&to.path !== '/login'){
        next('/login');
    }else if(to.meta.permission){
        // // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        // role === 'admin' ? next() : next('/403');
        next();
    }else{
      
        // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
        if(navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor'){
            Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
                confirmButtonText: '确定'
            });
        }else{
            next();
        }
    }
})


//定义常用参数
Vue.prototype.HOSPITAL = -1;
Vue.prototype.USERMAP = -1;

/**
 * set
 */
Vue.prototype.setHospital = function (hospital){
  Vue.prototype.HOSPITAL = hospital;
}

Vue.prototype.setUserMap = function (usermap){
  Vue.prototype.USERMAP = usermap;
}
Vue.prototype.checkIsNull = function (str){
    if(str == null || str == undefined){
        return false;
    }
    return true;
}
Vue.prototype.getUrl = function (path,that){
    var th = this;
    this.HttpUtil.encryptGet(
        this,
        this.Global.URLPATH.GET_IMG_URL,
        {
        path:path
        },
        function (data) { 
            that.form.avatar = data.data
            th.avatarUrl = data.data
        },
        function (data) {//访问失败
          that.$message.error(data.msg);
        });
}
Vue.prototype.avatarUrl ; 
Date.prototype.format = function(format){
    var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(), //day
    "h+" : this.getHours(), //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3), //quarter
    "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
        }
    for(var k in o) {
        if(new RegExp("("+ k +")").test(format)) {
        format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
        }
    }
    return format;
}
   
Date.prototype.parse =  function(dateStr,separator){
    if(!separator){
           separator="-";
    }
    var dateArr = dateStr.split(separator);
    var year = parseInt(dateArr[0]);
    var month;
    //处理月份为04这样的情况                         
    if(dateArr[1].indexOf("0") == 0){
        month = parseInt(dateArr[1].substring(1));
    }else{
         month = parseInt(dateArr[1]);
    }
    var day = parseInt(dateArr[2]);
    var date = new Date(year,month -1,day);
    return date;
}
   
new Vue({
    router,
    render: h => h(App)
}).$mount('#app');

