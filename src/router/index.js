import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
	routes: [{
			path: '/',
			redirect: '/dashboard'
		},
		{
			path: '/',
			component: resolve => require(['../components/common/Home.vue'], resolve),
			meta: {
				title: '自述文件'
			},
			children: [{
					path: '/dashboard',
					name: '/home',
					component: resolve => require(['../components/page/Dashboard.vue'], resolve),
					meta: {
						title: '系统首页'
					},

				},
				{
					path: '/table',
					component: resolve => require(['../components/page/BaseTable.vue'], resolve),
					meta: {
						title: '基础表格'
					}
				},
				{
					path: '/tabs',
					component: resolve => require(['../components/page/Tabs.vue'], resolve),
					meta: {
						title: 'tab选项卡'
					}
				},
				{
					path: '/form',
					component: resolve => require(['../components/page/BaseForm.vue'], resolve),
					meta: {
						title: '基本表单'
					}
				},
				{
					// 富文本编辑器组件
					path: '/editor',
					component: resolve => require(['../components/page/VueEditor.vue'], resolve),
					meta: {
						title: '富文本编辑器'
					}
				},
				{
					// markdown组件
					path: '/markdown',
					component: resolve => require(['../components/page/Markdown.vue'], resolve),
					meta: {
						title: 'markdown编辑器'
					}
				},
				{
					// 图片上传组件
					path: '/upload',
					component: resolve => require(['../components/page/Upload.vue'], resolve),
					meta: {
						title: '文件上传'
					}
				},
				{
					// vue-schart组件
					path: '/charts',
					component: resolve => require(['../components/page/BaseCharts.vue'], resolve),
					meta: {
						title: 'schart图表'
					}
				},
				{
					path: '/icon',
					component: resolve => require(['../components/page/Icon.vue'], resolve),
					meta: {
						title: '自定义图标'
					}
				},
				{
					// 拖拽列表组件
					path: '/drag',
					component: resolve => require(['../components/page/DragList.vue'], resolve),
					meta: {
						title: '拖拽列表'
					}
				},
				{
					// 权限页面
					path: '/permission',
					component: resolve => require(['../components/page/Permission.vue'], resolve),
					meta: {
						title: '权限测试',
					}
				},

				{
					// 部门管理
					path: '/dept',
					component: resolve => require(['../components/page/dept/DeptList.vue'], resolve),
					meta: {
						title: '厂管理',
					},
				},
				{
					path: '/dept/update/:id',
					name: '/dept/update',
					component: resolve => require(['../components/page/dept/Update.vue'], resolve),
					meta: {
						title: '修改部门'
					}
				},
				{
					path: '/dept/add',
					name: '/dept/add',
					component: resolve => require(['../components/page/dept/Add.vue'], resolve),
					meta: {
						title: '添加部门'
					}
				},
				{
					path: '/user',
					name: 'user',
					component: resolve => require(['../components/page/user/UserList.vue'], resolve),
					meta: {
						title: '介绍人管理'
					}
				}, {
					path: '/business',
					name: 'business',
					component: resolve => require(['../components/page/business/index.vue'], resolve),
					meta: {
						title: '商家管理'
					}
				}, 
				{
					path: '/test1',
					name: 'test1',
					component: resolve => require(['../components/page/business/test.vue'], resolve),
					meta: {
						title: '测试管理'
					}
				}, 
				{
					path: '/business/add',
					name: 'addBusiness',
					component: resolve => require(['../components/page/business/add.vue'], resolve),
					meta: {
						title: '商家新增'
					}
				}, {
					path: '/business/edit/:id',
					name: 'editBusiness',
					component: resolve => require(['../components/page/business/add.vue'], resolve),
					meta: {
						title: '商家编辑'
					}
				},
				{
					path: '/recruit/',
					name: 'recruit',
					component: resolve => require(['../components/page/recruit/index.vue'], resolve),
					meta: {
						title: '招聘管理'
					}
				},
				{
					path: '/recruit/add/:id/:name',
					name: 'addEmploy',
					component: resolve => require(['../components/page/recruit/add.vue'], resolve),
					meta: {
						title: '新增招聘'
					}
				},
				{
					path: '/recruit/edit/:id',
					name: 'editEmploy',
					component: resolve => require(['../components/page/recruit/add.vue'], resolve),
					meta: {
						title: '编辑招聘'
					}
				},
				{
					path: '/deliver/:id',
					name: 'deliver',
					component: resolve => require(['../components/page/recruit/deliver.vue'], resolve),
					meta: {
						title: '简历管理'
					}
				},
				{
					path: '/userManager',
					name: 'userManager',
					component: resolve => require(['../components/page/userManager/UserManagerList.vue'], resolve),
					meta: {
						title: '负责人管理'
					}
				},
				{
					path: '/stationing',
					component: resolve => require(['../components/page/userManager/StationingList.vue'], resolve),
					meta: {
						title: '驻厂管理'
					}
				},
				{
					path: '/update_user',
					component: resolve => require(['../components/page/user/UpdateUser.vue'], resolve),
					meta: {
						title: '用户列表'
					}
				},
				{
					path: '/quest',
					component: resolve => require(['../components/page/quest/QuestList.vue'], resolve),
					meta: {
						title: '问题管理'
					}
				},
				{
					path: '/quest/detail',
					component: resolve => require(['../components/page/quest/Detail.vue'], resolve),
					meta: {
						title: '问题详情'
					}
				},

				{
					// 组织
					path: '/mechanism/index',
					component: resolve => require(['../components/page/mechanism/MechanismList.vue'], resolve),
					meta: {
						title: '机构管理',
					},
				},
				{
					// 组织添加
					path: '/mechanism/add',
					component: resolve => require(['../components/page/mechanism/Add.vue'], resolve),
					meta: {
						title: '机构添加',
					},
				},
				{
					// 组织修改
					path: '/mechanism/update',
					component: resolve => require(['../components/page/mechanism/Update.vue'], resolve),
					meta: {
						title: '机构修改',
					},
				},
				{
					// 班次管理
					path: '/rule/index',
					component: resolve => require(['../components/page/rule/RuleList.vue'], resolve),
					meta: {
						title: '规则制定',
					},
				},
				{
					// 规则制定
					path: '/rule/rule',
					name: 'rule',
					component: resolve => require(['../components/page/rule/Rule.vue'], resolve),
					meta: {
						title: '规则制定',
					},
				},
				// {
				// 	// 角色管理
				// 	path: '/role/index',
				// 	name: 'rule',
				// 	component: resolve => require(['../components/page/role/RoleList.vue'], resolve),
				// 	meta: {
				// 		title: '角色管理',
				// 		permission: true
				// 	},
				// },
				{
					// 角色管理-添加
					path: '/role/add',
					name: 'role',
					component: resolve => require(['../components/page/role/Add.vue'], resolve),
					meta: {
						title: '角色添加',
					},
				},
				{
					// 角色管理-修改
					path: '/role/update',
					name: 'role/update',
					component: resolve => require(['../components/page/role/Update.vue'], resolve),
					meta: {
						title: '角色修改',
					},
				},
				{
					// 角色管理-权限分配
					path: '/role/menuPower',
					name: 'role/menuPower',
					component: resolve => require(['../components/page/role/MenuPower.vue'], resolve),
					meta: {
						title: '权限分配',
					},
				},

				{
					// 菜单管理
					path: '/role/menu',
					name: '/role/menu',
					component: resolve => require(['../components/page/menu/menuList.vue'], resolve),
					meta: {
						title: '菜单管理',
					},
				},
				{
					// 员工管理
					path: '/staffManagement',
					name: 'staffManagement',
					component: resolve => require(['../components/page/userManager/StaffManagement.vue'], resolve),
					meta: {
						title: '员工管理',

					},
				},
				// {
				//     // 测试日历
				//     path: '/calendar/test',
				//     component: resolve => require(['../components/page/calendar/Test.vue'], resolve),
				//     meta: { title: '测试日历', permission: true },
				// }

			]
		},
		{
			path: '/user',
			redirect: '/user'
		},
		{
			path: '/user',
			component: resolve => require(['../components/common/Home.vue'], resolve),
			meta: {
				title: '个人中心'
			},
			children: [{
				// 修改密码界面
				path: '/update_password',
				component: resolve => require(['../components/page/user/UpdatePassword.vue'], resolve),
				meta: {
					title: '修改密码',
				}
			}]
		},
		{
			path: '/login',
			component: resolve => require(['../components/page/Login.vue'], resolve)
		},
		{
			path: '/404',
			component: resolve => require(['../components/page/404.vue'], resolve)
		},
		{
			path: '/403',
			component: resolve => require(['../components/page/403.vue'], resolve)
		},
		{
			path: '*',
			redirect: '/404'
		}
	]
})
